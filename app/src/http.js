var storage = require('./storage.js');
var Vue = require('vue');

module.exports = function() {
    settings = storage.settings;
    Vue.http.options.root = '/' + settings.db;
    Vue.http.headers.custom.Authorization = 'bearer ' + settings.key;
    Vue.http.headers.custom['X-Employee'] = storage.employee || '';
};
