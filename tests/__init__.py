try:
    from trytond.modules.flask_shipment_in_app.tests.test_flask_shipment_in_app import suite  # noqa: E501
except ImportError:
    from .test_flask_shipment_in_app import suite

__all__ = ['suite']
