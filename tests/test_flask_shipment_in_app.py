# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest

from trytond.tests.test_tryton import ModuleTestCase
from trytond.tests.test_tryton import suite as test_suite


class FlaskShipmentInAppTestCase(ModuleTestCase):
    'Test Flask Shipment In App module'
    module = 'flask_shipment_in_app'


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            FlaskShipmentInAppTestCase))
    return suite
